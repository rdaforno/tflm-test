## Basic example (sine model)

Based on 'hello world': https://github.com/tensorflow/tensorflow/tree/master/tensorflow/lite/micro/examples/hello_world


## TensorFlow Lite Micro links

Repo: https://github.com/tensorflow/tensorflow/tree/master/tensorflow/lite/micro  
Getting started: https://www.tensorflow.org/lite/microcontrollers/get_started_low_level  
Our tensorflow repo: https://gitlab.ethz.ch/tec/research/tensorflow  


## Training

very useful jupyter notebook, step-by-step introduction to training a model:  
https://github.com/tensorflow/tensorflow/blob/master/tensorflow/lite/micro/examples/hello_world/train/train_hello_world_model.ipynb  
download pre-trained model:
https://github.com/tensorflow/tensorflow/tree/master/tensorflow/lite/micro/examples/hello_world/train


## Conversion

After generating the model, use the tensorflow lite converter: https://www.tensorflow.org/lite/convert  
-> generates an optimized flatbuffer format identified by the .tflite extension  

```
  import tensorflow as tf
  converter = tf.lite.TFLiteConverter.from_saved_model(saved_model_dir)
  tflite_model = converter.convert()
  open('model.tflite', 'wb').write(tflite_model)
```

Flatbuffer format can be directly used on the MCU, it just needs to be converted to a C array:
```
  xxd -i model.tflite > model.h
```
