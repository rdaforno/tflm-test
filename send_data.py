#!/usr/bin/env python3

# sends a single data item (float value) to the MCU for inference

import serial
import time
import sys
import binascii
import struct

port	 = '/dev/ttyUSB1'
baudrate = 1000000
inputval = 0.5

frame  = b'\x7e'
escape = b'\x7d'


def connect_to_serial(p, br):
    try:
        sp = serial.Serial(port=p, baudrate=br, timeout=None)
        sp.flushInput()
        sp.flushOutput()
        print("connected to %s" % p)
        return sp
    except SerialException:
        print("device %s unavailable" % p)
    except ValueError:
        print("invalid arguments")
    except OSError:
        print("device %s not found" % p)
    except:
        e = sys.exc_info()[0]
        print("error: %s" % e)
    return None


if len(sys.argv) > 1:
    try:
        inputval = float(sys.argv[1])
    except:
        print("non-float argument ignored")

sp = connect_to_serial(port, baudrate)
if sp:
    sp.setDTR(False)	# make sure the reset line of the MCU is released
    time.sleep(0.5)	# give the MCU some time to start up
    sp.flushInput()	# discard input
    # send the input value
    print("sending input value %.3f..." % inputval)
    # compose the packet
    payload = ("%.3f" % inputval).encode()
    crc     = binascii.crc32(payload)
    seqno   = 0
    dataid  = 0
    header  = struct.pack('<HHI', seqno, dataid, crc)
    pkt     = header + payload
    # mask (escape) all occurrances of framing and escape byte within the packet
    pkt_esc = pkt.replace(escape, escape + escape).replace(frame, escape + frame)
    pkt_fr  = frame + pkt_esc + frame
    #print(pkt_esc)
    #print("crc: %s" % (hex(crc % (1<<32))))
    sp.write(pkt_fr)
    time.sleep(0.1)	# give the MCU some time to run the inference
    if sp.inWaiting() > 0:
        print(sp.read(sp.inWaiting()).decode('UTF-8'))
    sp.close()
