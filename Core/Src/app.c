
#include "main.h"
#include "model.h"

#define TFLM_NETWORK_TENSOR_AREA_SIZE   1408
#define TFLM_NETWORK_MODEL              model_tflite

#define FRAMING_BYTE                    0x7e
#define ESCAPE_BYTE                     0x7d
#define RX_BUFFER_SIZE                  1024
#define UART_PKT_HDR_LEN                8


typedef struct __attribute__((packed))
{
  uint16_t seq_no;
  uint16_t data_id;
  uint32_t crc;
  uint8_t  payload[RX_BUFFER_SIZE - UART_PKT_HDR_LEN];
} uart_pkt_t;

extern UART_HandleTypeDef huart1;

__attribute__((aligned(16))) static uint8_t  tensor_arena[TFLM_NETWORK_TENSOR_AREA_SIZE];

static uint32_t model_hdl    = 0;
static float*   model_input  = 0;
static float*   model_output = 0;


bool APP_Init(void)
{
  /* instantiate a TFLM interpreter */
  if (tflm_c_create(TFLM_NETWORK_MODEL, (uint8_t*)tensor_arena, TFLM_NETWORK_TENSOR_AREA_SIZE, &model_hdl) != kTfLiteOk) {
    return false;
  }
  if (tflm_c_inputs_size(model_hdl) != 1) {
    printf("ERROR invalid model input size\r\n");
    return false;
  }

  /* print some infos about the neural network */
  printf("Network parameters:\r\n"
         "  operator size:    %lu\r\n"
         "  tensor size:      %lu\r\n"
         "  input size:       %lu\r\n"
         "  output size:      %lu\r\n\r\n"
         "  allocated memory: %lu of %lu\r\n",
         (uint32_t)tflm_c_operators_size(model_hdl),
         (uint32_t)tflm_c_tensors_size(model_hdl),
         (uint32_t)tflm_c_inputs_size(model_hdl),
         (uint32_t)tflm_c_outputs_size(model_hdl),
         (uint32_t)tflm_c_arena_used_bytes(model_hdl),
         (uint32_t)TFLM_NETWORK_TENSOR_AREA_SIZE);

  /* get a pointer to the input and output buffers */
  struct tflm_c_tensor_info t_info;
  tflm_c_input(model_hdl, 0, &t_info);
  model_input = t_info.data;
  tflm_c_output(model_hdl, 0, &t_info);
  model_output = t_info.data;

  return true;
}


static bool APP_ParseData(uart_pkt_t* pkt, uint32_t len)
{
#define PI    3.1415926535f

  static uint16_t last_seq_no = 0;

  /* check payload length */
  if (len <= UART_PKT_HDR_LEN) {
    UART_PRINTLN("packet too small");
    return false;
  }

  /* calculate and compare the CRC */
  uint32_t crc_calc = crc32(pkt->payload, len - UART_PKT_HDR_LEN, 0);
  if (crc_calc != pkt->crc) {
    UART_PRINTF("invalid CRC, packet dropped (0x%x vs 0x%x)\r\n", crc_calc, pkt->crc);
    return false;
  }

  /* check sequence number */
  if ((uint16_t)(pkt->seq_no - last_seq_no) > 1) {
    UART_PRINTF("gap in sequence number (%u packets missed?)\r\n", (uint16_t)(pkt->seq_no - last_seq_no));
  }
  last_seq_no = pkt->seq_no;

  /* convert payload to float */
  float val = atof((char*)pkt->payload);
  UART_PRINTF("seqno: %u, dataid: %u, value: %f\r\n", pkt->seq_no, pkt->data_id, val);
  while (val > (2 * PI)) val -= 2 * PI;   /* keep the value within the range [0;2*PI] */
  *model_input = val;

  return true;    // true = abort input parsing and run inference
}


static void APP_ReceiveData(void)
{
  static uint8_t rx_buffer[RX_BUFFER_SIZE + 1] __attribute__ ((aligned (32)));

  bool     start_found = false;
  bool     escaped     = false;
  uint32_t len         = 0;

  /* read data from serial port */
  while (1) {
    /* wait for the next character */
    uint8_t rcv = 0;
    if (HAL_UART_Receive(&huart1, &rcv, 1, 10) == HAL_OK) {
      /* character received */
      if (!escaped && (rcv == ESCAPE_BYTE)) {
        escaped = true;
      } else {
        if (!start_found) {
          len = 0;
          if (!escaped && (rcv == FRAMING_BYTE)) {
            start_found = true;
          }
        } else {
          if (!escaped && (rcv == FRAMING_BYTE)) {
            /* frame start / end */
            if (len > 0) {
              rx_buffer[len] = 0;
              UART_PRINTF("packet of size %u bytes received\r\n", len);
              if (APP_ParseData((uart_pkt_t*)rx_buffer, len)) {
                break;    /* abort data reception */
              }
              len         = 0;
              start_found = false;
            } // else: invalid packet length -> ignore
          } else {
            rx_buffer[len++] = rcv;
          }

          /* check for buffer overflow */
          if (len >= RX_BUFFER_SIZE) {
            // packet is too long
            UART_PRINTLN("packet dropped (too long)");
            len         = 0;
            start_found = false;
          }
        }
        escaped = false;
      }
    } // else: most likely a timeout
  }
}


void APP_Run(void)
{
  while (1) {
    APP_ReceiveData();

    if (model_hdl) {
      float val = *model_input;
      tflm_c_invoke(model_hdl);
      printf("model output and absolute error: sin(%.2f) = %+.3f (%.3f)\r\n", val, *model_output, fabsf(sin(val) - *model_output));
    }
  }
}

